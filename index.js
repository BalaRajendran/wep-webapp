const express = require("express");
const mongoose = require("mongoose");
const keys = require("./config/keys");
const bodyParser = require("body-parser");
const cors = require("cors");
const sgMail = require("@sendgrid/mail");
const router = express.Router();
const emailTemplate = require("./emailTemplate");
const contactUsSchema = require("./contactUsSchema");

sgMail.setApiKey(keys.SENDGRID_API_KEY);

mongoose.Promise = global.Promise;
mongoose.connect(
  keys.mongoURI,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (err, db) {
    if (!err) {
      console.log("Database Connected!");
    } else {
      console.log("Database Not Connected!");
    }
  }
);
const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//
router.route("/api").get(function (req, res, next) {
  res.send("s");
});

router.route("/api/contact_us").post(async function (req, res, next) {
  const { name, email, phone, message } = req.body;
  const contactUs = new contactUsSchema({
    name,
    email,
    phone,
    message,
  });
  const msg = {
    to: email,
    from: "wepmps@gmail.com",
    fromname: name,
    reply_to: "wepmps@gmail.com",
    subject: "Message from Wep MPS",
    text: "Thanks for contacting us",
    html: emailTemplate.htmltemplate1,
  };
  try {
    const contactToDb = await contactUs.save();
    const sendEmailToUser = await sgMail.send(msg);
    res.send("Thank you for your Response!");
  } catch (err) {
    console.log(err);
    res.send("something wrong");
  }
});

app.use("/", router);

//
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build/"));
  const path = require("path");
  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

const PORT = process.env.PORT || 5000;
var listener = app.listen(PORT, function () {
  console.log("Server Started on port " + listener.address().port);
});

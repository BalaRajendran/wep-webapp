import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router";
import SectionsScreen from "./screens/SectionsPage/SectionsPage";
import * as serviceWorker from "./serviceWorker";
import "./index.css";

import "./assets/scss/material-kit-pro-react.scss?v=1.3.0";
import "animate.css/animate.min.css";

var hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/" component={SectionsScreen} />
    </Switch>
  </Router>,
  document.getElementById("root")
);

serviceWorker.register();

import React, { Component } from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
// core components
import Card from "./../../components/Card/Card.jsx";
// sections of this Page
import SectionHeaders from "./Sections/SectionHeaders.jsx";
import Favorite from "@material-ui/icons/Favorite";
import SectionFeatures from "./Sections/SectionFeatures.jsx";
import SectionBlogs from "./Sections/SectionBlogs.jsx";
import SectionProjects from "./Sections/SectionProjects.jsx";
import SectionTestimonials from "./Sections/SectionTestimonials.jsx";
import SectionCarousel from "./Sections/SectionCarousel";
import SectionContacts from "./Sections/SectionContacts.jsx";
import SectionClient from "./Sections/SectionClient.jsx";
import sectionsPageStyle from "./../../assets/jss/material-kit-pro-react/views/sectionsPageStyle.jsx";
import Footer from "../../components/Footer/Footer.jsx";

class SectionsPage extends Component {
  componentDidMount() {
    var href = window.location.href.substring(
      window.location.href.lastIndexOf("#/") + 2
    );
    var hrefId = href.substring(href.lastIndexOf("#") + 1);
    if (href.lastIndexOf("#") > 0) {
      document.getElementById(hrefId).scrollIntoView();
    }
    window.addEventListener("scroll", this.updateView());
    this.updateView();
    return function cleanup() {
      window.removeEventListener("scroll", this.updateView());
    };
  }
  updateView = () => {
    var contentSections = document.getElementsByClassName("cd-section");
    var navigationItems = document
      .getElementById("cd-vertical-nav")
      .getElementsByTagName("a");

    for (let i = 0; i < contentSections.length; i++) {
      var activeSection =
        parseInt(navigationItems[i].getAttribute("data-number"), 10) - 1;
      if (
        contentSections[i].offsetTop - window.innerHeight / 2 <
          window.pageYOffset &&
        contentSections[i].offsetTop +
          contentSections[i].scrollHeight -
          window.innerHeight / 2 >
          window.pageYOffset
      ) {
        navigationItems[activeSection].classList.add("is-selected");
      } else {
        navigationItems[activeSection].classList.remove("is-selected");
      }
    }
  };
  render() {
    const easeInOutQuad = (t, b, c, d) => {
      t /= d / 2;
      if (t < 1) return (c / 2) * t * t + b;
      t--;
      return (-c / 2) * (t * (t - 2) - 1) + b;
    };
    const smoothScroll = (target) => {
      console.log(target, document.getElementById(target));
      var targetScroll = document.getElementById(target);
      scrollGo(document.documentElement, targetScroll.offsetTop + 700, 1250);
    };
    const scrollGo = (element, to, duration) => {
      var start = element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 20;

      var animateScroll = function() {
        currentTime += increment;
        var val = easeInOutQuad(currentTime, start, change, duration);
        element.scrollTop = val;
        if (currentTime < duration) {
          setTimeout(animateScroll, increment);
        }
      };
      animateScroll();
    };
    const { classes } = this.props;
    // const useStyles = makeStyles(sectionsPageStyle);
    return (
      <div>
        <div className={classes.main}>
          <div>
            <SectionHeaders />
            <SectionCarousel />
            <Card>
              <SectionBlogs />
              <SectionFeatures />
              <SectionProjects />
              <SectionTestimonials />
              <SectionClient />
              <SectionContacts />
            </Card>
            <div>
              <Footer
                content={
                  <div>
                    <div className={classes.right}>
                      Site is maintained by
                      <Favorite className={classes.icon} />
                      <a
                        without_rel="noopener noreferrer"
                        href="https://www.wepmps.com/"
                      >
                        WEP
                      </a>
                    </div>
                  </div>
                }
              />
            </div>
          </div>
          <nav id="cd-vertical-nav">
            <ul>
              <li>
                <a
                  href="#home"
                  data-number="1"
                  className="is-selected"
                  onClick={(e) => {
                    var isMobile = navigator.userAgent.match(
                      /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                    );
                    if (isMobile) {
                      // if we are on mobile device the scroll into view will be managed by the browser
                    } else {
                      e.preventDefault();
                      smoothScroll("home");
                    }
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Home</span>
                </a>
              </li>
              <li>
                <a
                  href="#whymepmps"
                  data-number="2"
                  className=""
                  onClick={(e) => {
                    var isMobile = navigator.userAgent.match(
                      /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                    );
                    if (isMobile) {
                      // if we are on mobile device the scroll into view will be managed by the browser
                    } else {
                      e.preventDefault();
                      smoothScroll("whymepmps");
                    }
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Why Wep MPS</span>
                </a>
              </li>
              <li>
                <a
                  href="#printservices"
                  data-number="3"
                  className=""
                  onClick={(e) => {
                    var isMobile = navigator.userAgent.match(
                      /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                    );
                    if (isMobile) {
                      // if we are on mobile device the scroll into view will be managed by the browser
                    } else {
                      e.preventDefault();
                      smoothScroll("printservices");
                    }
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Managed Print Services</span>
                </a>
              </li>
              <li>
                <a
                  href="#howitworks"
                  data-number="4"
                  className=""
                  onClick={(e) => {
                    var isMobile = navigator.userAgent.match(
                      /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                    );
                    if (isMobile) {
                      // if we are on mobile device the scroll into view will be managed by the browser
                    } else {
                      e.preventDefault();
                      smoothScroll("howitworks");
                    }
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">How it works</span>
                </a>
              </li>
              <li>
                <a
                  href="#mpsbenefits"
                  data-number="5"
                  className=""
                  onClick={(e) => {
                    var isMobile = navigator.userAgent.match(
                      /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                    );
                    if (isMobile) {
                      // if we are on mobile device the scroll into view will be managed by the browser
                    } else {
                      e.preventDefault();
                      smoothScroll("mpsbenefits");
                    }
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Mps Benefits</span>
                </a>
              </li>
              <li>
                <a
                  href="#pressrelease"
                  data-number="6"
                  className=""
                  onClick={(e) => {
                    var isMobile = navigator.userAgent.match(
                      /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                    );
                    if (isMobile) {
                      // if we are on mobile device the scroll into view will be managed by the browser
                    } else {
                      e.preventDefault();
                      smoothScroll("pressrelease");
                    }
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Press Release</span>
                </a>
              </li>
              <li>
                <a
                  href="#statistics"
                  data-number="7"
                  className=""
                  onClick={(e) => {
                    var isMobile = navigator.userAgent.match(
                      /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                    );
                    if (isMobile) {
                      // if we are on mobile device the scroll into view will be managed by the browser
                    } else {
                      e.preventDefault();
                      smoothScroll("statistics");
                    }
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Statistics</span>
                </a>
              </li>
              <li>
                <a
                  href="#testimonals"
                  data-number="7"
                  className=""
                  onClick={(e) => {
                    var isMobile = navigator.userAgent.match(
                      /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                    );
                    if (isMobile) {
                      // if we are on mobile device the scroll into view will be managed by the browser
                    } else {
                      e.preventDefault();
                      smoothScroll("testimonals");
                    }
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Testimonals</span>
                </a>
              </li>
              <li>
                <a
                  href="#contacts"
                  data-number="8"
                  className=""
                  onClick={(e) => {
                    var isMobile = navigator.userAgent.match(
                      /(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i
                    );
                    if (isMobile) {
                      // if we are on mobile device the scroll into view will be managed by the browser
                    } else {
                      e.preventDefault();
                      smoothScroll("contacts");
                    }
                  }}
                >
                  <span className="cd-dot" />
                  <span className="cd-label">Contact Us</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

export default withStyles(sectionsPageStyle)(SectionsPage);

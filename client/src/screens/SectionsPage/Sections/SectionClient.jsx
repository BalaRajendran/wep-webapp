import React from "react";
import Slider from "react-slick";
import withStyles from "@material-ui/core/styles/withStyles";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import carouselStyle from "./../../../assets/jss/material-kit-pro-react/views/componentsSections/carouselStyle.jsx";

import Paper from "@material-ui/core/Paper";
import ScrollAnimation from "react-animate-on-scroll";

import Card from "./../../../components/Card/Card.jsx";

class SectionCarousel extends React.Component {
  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 1000,
      slidesToShow: 3,
      slidesToScroll: 3,
      autoplay: true,
      pauseOnHover: true,
    };
    let sliderContent = [];
    [
      "/assets/img/clients/1.jpg",
      "/assets/img/clients/2.jpg",
      "/assets/img/clients/3.jpg",
      "/assets/img/clients/4.jpg",
      "/assets/img/clients/5.jpg",
    ].map((slider, i) => {
      return sliderContent.push(
        <GridItem key={i} xs={3} md={4} className={"slider-client"}>
          <ScrollAnimation animateIn="pulse">
            <Card profile plain>
              <Paper
                elevation={3}
                style={{
                  height: "100px",
                }}
              >
                <img alt="wep" src={slider} />
              </Paper>
            </Card>
          </ScrollAnimation>
        </GridItem>
      );
    });
    return (
      <div className="slider-client-mock">
        <h2
          style={{
            textAlign: "center",
            fontFamily: '"Roboto Slab", "Times New Roman", serif',
            fontWeight: "700",
            color: "#3C4858",
          }}
        >
          Our Clients
        </h2>
        <Slider {...settings} style={{ cursor: "pointer" }}>
          {sliderContent}
        </Slider>
      </div>
    );
  }
}

export default withStyles(carouselStyle)(SectionCarousel);

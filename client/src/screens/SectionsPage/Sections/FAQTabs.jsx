import React from "react";
import FAQresults from "./FAQresults.jsx";
import NavPills from "./../../../components/NavPills/NavPills.jsx";

const FAQTabs = () => {
  return (
    <NavPills
      alignCenter
      color="info"
      tabs={[
        {
          tabButton: "ALL FAQS",
          tabContent: <FAQresults />
        },
        {
          tabButton: "FAQ TAB2",
          tabContent: <FAQresults />
        },
        {
          tabButton: "FAQ TAB3",
          tabContent: <FAQresults />
        }
      ]}
    />
    // <FAQresults />
  );
};

export default FAQTabs;

import React from "react";
import GridContainer from "./../../../components/Grid/GridContainer.jsx";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import Accordion from "./../../../components/Accordion/Accordion.jsx";
const FAQquestions = [
  {
    question: "What if I want to renew my policy after one year?",
    answer:
      "The killdeer (Charadrius vociferus) is a large plover found in the Americas. It was described and given its current scientific name in 1758 by Carl Linnaeus in the 10th edition of his Systema Naturae. Subspecies breed from southeastern Alaska and southern Canada to Mexico, in the West Indies, and in and around Peru. The non-breeding habitat includes coastal wetlands, beach habitats, and coastal fields. Although it is a shorebird, it does not necessarily nest close to water. Both parents incubate their eggs for 22 to 28 days on average."
  },
  {
    question: "What is the maximum number of claims allowed in a year?",
    answer:
      "The killdeer (Charadrius vociferus) is a large plover found in the Americas. It was described and given its current scientific name in 1758 by Carl Linnaeus in the 10th edition of his Systema Naturae. Subspecies breed from southeastern Alaska and southern Canada to Mexico, in the West Indies, and in and around Peru. The non-breeding habitat includes coastal wetlands, beach habitats, and coastal fields. Although it is a shorebird, it does not necessarily nest close to water. Both parents incubate their eggs for 22 to 28 days on average."
  },
  {
    question: "What is the family floater plan?",
    answer:
      "The killdeer (Charadrius vociferus) is a large plover found in the Americas. It was described and given its current scientific name in 1758 by Carl Linnaeus in the 10th edition of his Systema Naturae. Subspecies breed from southeastern Alaska and southern Canada to Mexico, in the West Indies, and in and around Peru. The non-breeding habitat includes coastal wetlands, beach habitats, and coastal fields. Although it is a shorebird, it does not necessarily nest close to water. Both parents incubate their eggs for 22 to 28 days on average."
  },
  {
    question: "What is health card?",
    answer:
      "The killdeer (Charadrius vociferus) is a large plover found in the Americas. It was described and given its current scientific name in 1758 by Carl Linnaeus in the 10th edition of his Systema Naturae. Subspecies breed from southeastern Alaska and southern Canada to Mexico, in the West Indies, and in and around Peru. The non-breeding habitat includes coastal wetlands, beach habitats, and coastal fields. Although it is a shorebird, it does not necessarily nest close to water. Both parents incubate their eggs for 22 to 28 days on average."
  },
  {
    question: "What do you mean by cashless Hospitalisation?",
    answer:
      "The killdeer (Charadrius vociferus) is a large plover found in the Americas. It was described and given its current scientific name in 1758 by Carl Linnaeus in the 10th edition of his Systema Naturae. Subspecies breed from southeastern Alaska and southern Canada to Mexico, in the West Indies, and in and around Peru. The non-breeding habitat includes coastal wetlands, beach habitats, and coastal fields. Although it is a shorebird, it does not necessarily nest close to water. Both parents incubate their eggs for 22 to 28 days on average."
  },
  {
    question:
      "What are the points one should remember while going on cashless hospitalisation?",
    answer:
      "The killdeer (Charadrius vociferus) is a large plover found in the Americas. It was described and given its current scientific name in 1758 by Carl Linnaeus in the 10th edition of his Systema Naturae. Subspecies breed from southeastern Alaska and southern Canada to Mexico, in the West Indies, and in and around Peru. The non-breeding habitat includes coastal wetlands, beach habitats, and coastal fields. Although it is a shorebird, it does not necessarily nest close to water. Both parents incubate their eggs for 22 to 28 days on average."
  },
  {
    question: "What is a hospital under medical insurance?",
    answer:
      "The killdeer (Charadrius vociferus) is a large plover found in the Americas. It was described and given its current scientific name in 1758 by Carl Linnaeus in the 10th edition of his Systema Naturae. Subspecies breed from southeastern Alaska and southern Canada to Mexico, in the West Indies, and in and around Peru. The non-breeding habitat includes coastal wetlands, beach habitats, and coastal fields. Although it is a shorebird, it does not necessarily nest close to water. Both parents incubate their eggs for 22 to 28 days on average."
  }
];

const FAQresults = () => {
  return (
    <div>
      <GridContainer>
        <GridItem md={12} sm={12}>
          <Accordion
            active={0}
            activeColor="info"
            collapses={FAQquestions.map(prop => {
              return {
                title: prop.question,
                content: <p>{prop.answer}</p>
              };
            })}
          />
        </GridItem>
      </GridContainer>
    </div>
  );
};

export default FAQresults;

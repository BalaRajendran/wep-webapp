import React from "react";
import Carousel from "react-slick";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "./../../../components/Grid/GridContainer.jsx";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import carouselStyle from "./../../../assets/jss/material-kit-pro-react/views/componentsSections/carouselStyle.jsx";
import { Button } from "@material-ui/core";
// import KeyboardVoiceIcon from "@material-ui/icons/KeyboardVoice";

class SectionCarousel extends React.Component {
  render() {
    const { classes } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
    };
    let sliderContent = [];
    ["/assets/img/slider1.png", "/assets/img/slider1.png"].map((slider, i) => {
      return sliderContent.push(
        <div key={i}>
          <img src={slider} alt="First slide" className="slick-image" />
          <div className="slick-caption">
            <div
              style={{
                marginTop: "-439px",
                marginLeft: '469px'
              }}
              className="MuiGrid-root jss161 MuiGrid-item MuiGrid-grid-xs-12 MuiGrid-grid-sm-6 MuiGrid-grid-md-6"
            >
              <div>
                <h1 className="slider-data">
                  India's largest and most preferred MPS provider
                </h1>
              </div>
              <div style={{ textAlign: "start" }}>
                <Button className="book-btn">Book Free Assessment</Button>
                <Button className="book-btn"> Log a call</Button>
              </div>
            </div>
          </div>
        </div>
      );
    });
    return (
      <GridContainer id="home">
        <GridItem
          xs={12}
          sm={12}
          md={12}
          className={["set-height", classes.marginAuto].join(" ")}
        >
          <Carousel {...settings}>{sliderContent}</Carousel>
        </GridItem>
      </GridContainer>
    );
  }
}

export default withStyles(carouselStyle)(SectionCarousel);

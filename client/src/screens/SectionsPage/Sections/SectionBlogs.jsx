import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
// paper
import Paper from "@material-ui/core/Paper";
// core components
import GridContainer from "./../../../components/Grid/GridContainer.jsx";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import Card from "./../../../components/Card/Card.jsx";
import CardHeader from "./../../../components/Card/CardHeader.jsx";

import blogsStyle from "./../../../assets/jss/material-kit-pro-react/views/sectionsSections/blogsStyle.jsx";

// import office2 from "./../../../assets/img/office2.jpg";
import office2 from "./../../../assets/img/section2.png";
import ScrollAnimation from "react-animate-on-scroll";

/*Cards*/
import cardProfile1Square from "./../../../assets/img/faces/1.svg";
import cardProfile2Square from "./../../../assets/img/faces/2.svg";
import cardProfile3Square from "./../../../assets/img/faces/3.svg";
import cardProfile4Square from "./../../../assets/img/faces/4.svg";
import cardProfile5Square from "./../../../assets/img/faces/5.svg";

/*Pills*/
import Dashboard from "@material-ui/icons/Dashboard";
import Schedule from "@material-ui/icons/Schedule";
import NavPills from "./../../../components/NavPills/NavPills.jsx";

function SectionBlogs({ ...props }) {
  let cards = [];
  [
    cardProfile1Square,
    cardProfile2Square,
    cardProfile3Square,
    cardProfile4Square,
    cardProfile5Square,
  ].map((image, i) => {
    return cards.push(
      <GridItem key={i} xs={3} md={3}>
        <ScrollAnimation animateIn="pulse">
          <Card profile plain>
            <Paper
              elevation={3}
              style={{
                height: "100px",
                minWidth: "100px",
                Width: "100px",
                cursor: "pointer",
              }}
            >
              <img alt="wep" src={image} style={{ maxWidth: "99px" }} />
            </Paper>
          </Card>
        </ScrollAnimation>
      </GridItem>
    );
  });
  const { classes, ...rest } = props;
  return (
    <div className="cd-section" {...rest}>
      <div className={classes.blog} style={{ paddingTop: 0, paddingBottom: 0 }}>
        <div className={classes.container}>
          <div id="whymepmps">
            <GridContainer>
              <GridItem
                xs={12}
                sm={10}
                md={12}
                className={`${classes.mlAuto} ${classes.mrAuto}`}
              >
                <Card plain blog className={classes.card}>
                  <GridContainer>
                    <GridItem xs={12} sm={7} md={7}>
                      <h2 className={classes.cardTitle}>Why WeP MPS</h2>
                      <ScrollAnimation animateIn="pulse">
                        <h4 className={classes.description1}>
                          With the supply of eco-friendly printers, monitored
                          and controlled printing environment, and processing of
                          e-waste ensuring used printers, consumables, spares do
                          not go to the land fill if partnered with a
                          professional e-waste management company
                        </h4>
                      </ScrollAnimation>
                      <GridContainer style={{ justifyContent: "center" }}>
                        {cards}
                      </GridContainer>
                    </GridItem>
                    <GridItem xs={12} sm={5} md={5}>
                      <ScrollAnimation animateIn="fadeInRight">
                        <CardHeader image plain>
                          <a
                            href="#pablito"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              src={office2}
                              alt="..."
                              style={{
                                height: "480px",
                                marginBottom: "-18px",
                                padding: "10px",
                              }}
                            />
                          </a>
                          <div
                            className={classes.coloredShadow}
                            style={{
                              backgroundImage: `url(${office2})`,
                              opacity: "1",
                            }}
                          />
                        </CardHeader>
                      </ScrollAnimation>
                    </GridItem>
                  </GridContainer>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
          <div id="printservices">
            <GridContainer>
              <GridItem
                xs={12}
                sm={10}
                md={12}
                className={`${classes.mlAuto} ${classes.mrAuto}`}
              >
                <Card plain blog className={classes.card}>
                  <GridContainer>
                    <GridItem
                      xs={12}
                      sm={10}
                      md={12}
                      className={`${classes.mlAuto} ${classes.mrAuto}`}
                    >
                      <h2 className={classes.cardTitle}>
                        Managed Print Services
                      </h2>
                      <ScrollAnimation animateIn="pulse">
                        <h4 className={classes.description1}>
                          MPS being the next level opportunity of the printing
                          and imaging industry culminates the printing/ copying
                          /scanning requirements of organizations. The MPS
                          market is still under explored in India. despite India
                          being one of the fastest growing world markets in the
                          sector
                        </h4>
                      </ScrollAnimation>
                    </GridItem>
                    <GridItem md={12}>
                      <NavPills
                        color="primary"
                        horizontal={{
                          tabsGrid: { xs: 12, sm: 3, md: 4 },
                          contentGrid: { xs: 12, sm: 9, md: 8 },
                        }}
                        tabs={[
                          {
                            tabButton: "What is MPS?",
                            tabIcon: Dashboard,
                            tabContent: (
                              <span>
                                <h4>
                                  <strong>What is MPS? </strong>
                                </h4>
                                <h5>
                                  <ScrollAnimation animateIn="fadeIn">
                                    <b>Managed Print Serivces(MPS)</b> allow an
                                    organization to gain visibility and control
                                    of all its printing and optimize it, which
                                    helps save money, enables the organization
                                    to print more efficiently. Managed printing
                                    also helps the organization to improve
                                    environmental sustainability and document
                                    security.
                                  </ScrollAnimation>
                                </h5>

                                <br />
                                <h5>
                                  <ScrollAnimation animateIn="fadeIn">
                                    <b>Managed Print Serivces(MPS)</b> relates
                                    to the total cost of managing and optimizing
                                    an organization’s printers, their output,
                                    and the people and processes that support
                                    their devices (such as printers, scanners,
                                    plotters, MFPs, fax machines, etc.).
                                    Permissions and access levels can be defined
                                    separately for each group of users:
                                    employees, students, faculty, clients and
                                    other authorized personnel.
                                  </ScrollAnimation>
                                </h5>
                              </span>
                            ),
                          },
                          {
                            tabButton: "Why is MPS?",
                            tabIcon: Schedule,
                            tabContent: (
                              <span>
                                <h4>
                                  <strong>Why is MPS? </strong>
                                </h4>
                                <h5>
                                  As an MPS provider, WeP Solutions first
                                  understands the current pain areas of the
                                  clients in the management of printers and
                                  other imaging devices and then provides a
                                  customised solution to each of its clients.
                                  With the value propositions being provided by
                                  WeP Solutions, clients reap following
                                  benefits:
                                </h5>
                                <br />
                                <h5>
                                  Multi – location, Single-window Support:
                                </h5>
                                <h5>
                                  With many customers, a common problem
                                  identified is the multi-location support of
                                  printers by multiple vendors. WeP Solutions
                                  provides a single – window support with an
                                  easy call logging option which takes care of
                                  support across 2,000 locations across the
                                  country spanning to the remotest villages,
                                  with no extra cost to the client.
                                </h5>
                              </span>
                            ),
                          },
                        ]}
                      />
                    </GridItem>
                  </GridContainer>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withStyles(blogsStyle)(SectionBlogs);

import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import PinDrop from "@material-ui/icons/PinDrop";
import SnackbarContent from "./../../../components/Snackbar/SnackbarContent.jsx";
import Phone from "@material-ui/icons/Phone";
// core components
import GridContainer from "./../../../components/Grid/GridContainer.jsx";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import InfoArea from "./../../../components/InfoArea/InfoArea.jsx";
import Card from "./../../../components/Card/Card.jsx";
import CardHeader from "./../../../components/Card/CardHeader.jsx";
import axios from "axios";
import CardBody from "./../../../components/Card/CardBody.jsx";
import CardFooter from "./../../../components/Card/CardFooter.jsx";
import CustomInput from "./../../../components/CustomInput/CustomInput.jsx";
import Button from "./../../../components/CustomButtons/Button.jsx";
import classnames from "classnames";
import contactsStyle from "./../../../assets/jss/material-kit-pro-react/views/sectionsSections/contactsStyle.jsx";

// import city from "./../../../assets/img/examples/city.jpg";
import { CircularProgress } from "@material-ui/core";

const initialState = {
  erroMessage: "",
  name: "",
  phone: "",
  email: "",
  message: "",
  disableButton: false,
};

class SectionContacts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: [],
      ...initialState,
    };
  }
  handleToggle(value) {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked,
    });
  }

  handleInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      erroMessage: "",
    });
  };

  onSubmit = () => {
    this.setState({
      disableButton: true,
    });
    // eslint-disable-next-line
    const namePattern = /^[a-zA-Z. ]{2,50}$/;
    // eslint-disable-next-line
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // eslint-disable-next-line
    const phonePattern = /^([7-9]{1}[0-9]{9})$/;
    // eslint-disable-next-line
    const messagePattern = /^[a-zA-Z0-9.,! ]{2,200}$/;
    if (!namePattern.test(this.state.name)) {
      this.setState({
        disableButton: false,
        erroMessage: "Invalid Name@@@danger",
      });
    } else if (!emailPattern.test(this.state.email)) {
      this.setState({
        disableButton: false,
        erroMessage: "Invalid Email Id@@@danger",
      });
    } else if (!phonePattern.test(this.state.phone)) {
      this.setState({
        disableButton: false,
        erroMessage: "Invalid Phone Number@@@danger",
      });
    } else if (!messagePattern.test(this.state.message)) {
      this.setState({
        disableButton: false,
        erroMessage: "Invalid Message@@@danger",
      });
    } else {
      axios
        .post("/api/contact_us", {
          name: this.state.name,
          email: this.state.email,
          phone: this.state.phone,
          message: this.state.message,
        })
        .then(() => {
          this.setState({
            ...initialState,
            disableButton: false,
            erroMessage: "Thank you for contacting us@@@success",
          });
        });
    }
  };

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div className="cd-section" {...rest} id="contacts">
        <div
          className={`${classes.contacts} ${classes.section}`}
          style={{ backgroundImage: `url(/assets/img/contact.jpg)` }}
        >
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={5} md={5}>
                <h2 className={classes.title}>Get in Touch</h2>
                <h5 className={classes.description}>
                  You need more information? Check what other persons are saying
                  about our service. They are very happy with our service.
                </h5>
                <InfoArea
                  className={classes.infoArea}
                  title="Find us at the office"
                  description={
                    <span>
                      WeP Solutions Limited. <br />
                      40/1A, Basappa Complex, <br />
                      Lavelle Road, Bangalore-560001
                    </span>
                  }
                  icon={PinDrop}
                />
                <InfoArea
                  className={classes.infoArea}
                  title="Give us a ring"
                  description={
                    <span>
                      info@wepsol.in
                      <br /> 91-80-66112000
                    </span>
                  }
                  icon={Phone}
                />
              </GridItem>
              <GridItem xs={12} sm={5} md={5} className={classes.mlAuto}>
                <Card className={classes.card1}>
                  <form autoComplete="off">
                    <CardHeader
                      contact
                      color="primary"
                      className={classes.textCenter}
                    >
                      <h4 className={classes.cardTitle}>Contact Us</h4>
                    </CardHeader>
                    <CardBody>
                      <GridContainer>
                        <GridItem xs={12} sm={12} md={12}>
                          {this.state.erroMessage && (
                            <SnackbarContent
                              message={
                                <span>
                                  <b>
                                    {this.state.erroMessage.split("@@@")[0]}
                                  </b>
                                </span>
                              }
                              close
                              color={this.state.erroMessage.split("@@@")[1]}
                              icon="info_outline"
                            />
                          )}
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                          <CustomInput
                            name="name"
                            value={this.state.name}
                            state={(e) => {
                              this.handleInput(e);
                            }}
                            labelText="First Name"
                            id="name"
                            formControlProps={{
                              fullWidth: true,
                            }}
                          />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                          <CustomInput
                            name="email"
                            value={this.state.email}
                            state={(e) => {
                              this.handleInput(e);
                            }}
                            labelText="Email Address"
                            id="email"
                            formControlProps={{
                              fullWidth: true,
                            }}
                          />
                        </GridItem>
                      </GridContainer>

                      <CustomInput
                        name="phone"
                        value={this.state.phone}
                        state={(e) => {
                          this.handleInput(e);
                        }}
                        labelText="Phone Number"
                        id="phone"
                        formControlProps={{
                          fullWidth: true,
                        }}
                      />
                      <CustomInput
                        labelText="Your Message"
                        id="message"
                        name="message"
                        value={this.state.message}
                        state={(e) => {
                          this.handleInput(e);
                        }}
                        formControlProps={{
                          fullWidth: true,
                        }}
                        inputProps={{
                          multiline: true,
                          rows: 5,
                        }}
                      />
                    </CardBody>
                    <CardFooter className={classes.justifyContentBetween}>
                      <Button
                        color="primary"
                        onClick={this.onSubmit}
                        disabled={this.state.disableButton}
                        className={classnames(
                          [classes.pullRight, "make-hover-feedback"].join(" "),
                          {
                            "make-hover-feedback-hover": !this.state
                              .disableButton,
                          },
                          {
                            "make-hover-feedback-disable": this.state
                              .disableButton,
                          }
                        )}
                      >
                        {this.state.disableButton ? (
                          <CircularProgress
                            size={20}
                            style={{
                              color: !this.state.disableButton
                                ? "#fff"
                                : "#9c27b0",
                            }}
                          />
                        ) : (
                          "Send Message"
                        )}
                      </Button>
                    </CardFooter>
                  </form>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(contactsStyle)(SectionContacts);

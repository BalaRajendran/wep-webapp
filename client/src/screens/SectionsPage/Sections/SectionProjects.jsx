/* eslint-disable */
import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import Code from "@material-ui/icons/Code";
import Timeline from "@material-ui/icons/Timeline";
import Group from "@material-ui/icons/Group";
// core components
import GridContainer from "./../../../components/Grid/GridContainer.jsx";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import Card from "./../../../components/Card/Card.jsx";
import CardBody from "./../../../components/Card/CardBody.jsx";
import CardHeader from "./../../../components/Card/CardHeader.jsx";
import ScrollAnimation from "react-animate-on-scroll";
import InfoArea from "./../../../components/InfoArea/InfoArea.jsx";
import Badge from "./../../../components/Badge/Badge.jsx";

import projectsStyle from "./../../../assets/jss/material-kit-pro-react/views/sectionsSections/projectsStyle.jsx";

import cardProject5 from "./../../../assets/img/examples/card-project5.jpg";

function SectionProjects({ ...props }) {
  const { classes, ...rest } = props;
  return (
    <div className="cd-section" {...rest}>
      <div
        className={classes.projects}
        style={{ paddingTop: "95px", paddingBottom: 0 }}
        id="pressrelease"
      >
        <div className={classes.container}>
          <GridContainer>
            <GridItem
              xs={12}
              sm={8}
              md={8}
              className={`${classes.mlAuto} ${classes.mrAuto} ${classes.textCenter}`}
            >
              <h2 className={classes.title}>Press Release</h2>
              <h4 className={classes.description}>
                This is the press releaes where we have most awarded for MPS.
              </h4>
              <div className={classes.sectionSpace} />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={4} md={3}>
              <ScrollAnimation animateIn="fadeInLeft">
                <Card
                  plain
                  className={[classes.card2, "cursor-pointer"].join(" ")}
                >
                  <CardHeader image plain>
                    <img src={"/assets/img/press1.jpg"} alt="..." />
                    <div
                      className={classes.coloredShadow}
                      style={{
                        // backgroundImage:
                        //   "url('https://s3.amazonaws.com/creativetim_bucket/products/83/original/opt_mk_react_thumbnail.jpg?1525851474')",
                        opacity: "1",
                      }}
                    />
                  </CardHeader>
                  <CardBody plain>
                    <h6 className={classes.cardTitle}>
                      Technological Revolution through Managed Print Services
                    </h6>
                    <h6 className={classes.description}>BussinessWorld</h6>
                  </CardBody>
                </Card>
              </ScrollAnimation>
            </GridItem>
            <GridItem xs={12} sm={4} md={3}>
              <ScrollAnimation animateIn="fadeInLeft">
                <Card
                  plain
                  className={[classes.card2, "cursor-pointer"].join(" ")}
                >
                  <CardHeader image plain>
                    <img src="/assets/img/press2.jpg" alt="..." />
                    <div
                      className={classes.coloredShadow}
                      style={{
                        // backgroundImage:
                        //   "url('https://s3.amazonaws.com/creativetim_bucket/products/66/thumb/opt_lbdp_react_thumbnail.jpg?1509466309')",
                        opacity: "1",
                      }}
                    />
                  </CardHeader>
                  <CardBody plain>
                    <h6 className={classes.cardTitle}>
                      WeP Solution Ltd becomes the first Indian Company to offer
                      Secure Printing Solutions
                    </h6>
                    <h6 className={classes.description}>DataQuest</h6>
                  </CardBody>
                </Card>
              </ScrollAnimation>
            </GridItem>
            <GridItem xs={12} sm={4} md={3}>
              <ScrollAnimation animateIn="fadeInRight">
                <Card
                  plain
                  className={[classes.card2, "cursor-pointer"].join(" ")}
                >
                  <CardHeader image plain>
                    <img src="/assets/img/press3.jpg" alt="..." />
                    <div
                      className={classes.coloredShadow}
                      style={{
                        // backgroundImage:
                        //   "url('https://s3.amazonaws.com/creativetim_bucket/products/73/original/opt_nudp_react_thumbnail.jpg?1518533306')",
                        opacity: "1",
                      }}
                    />
                  </CardHeader>
                  <CardBody plain>
                    <h6 className={classes.cardTitle}>
                      WeP wins India's Best MPS Company Award for its innovative
                      services
                    </h6>
                    <h6 className={classes.description}>IT Voice</h6>
                  </CardBody>
                </Card>
              </ScrollAnimation>
            </GridItem>
            <GridItem xs={12} sm={4} md={3}>
              <ScrollAnimation animateIn="fadeInRight">
                <Card
                  plain
                  className={[classes.card2, "cursor-pointer"].join(" ")}
                >
                  <CardHeader image plain>
                    <img src="/assets/img/press4.jpg" alt="..." />
                    <div
                      className={classes.coloredShadow}
                      style={{
                        // backgroundImage:
                        //   "url('https://s3.amazonaws.com/creativetim_bucket/products/73/original/opt_nudp_react_thumbnail.jpg?1518533306')",
                        opacity: "1",
                      }}
                    />
                  </CardHeader>
                  <CardBody plain>
                    <h6 className={classes.cardTitle}>
                      Why India is a goldmine for MPS
                    </h6>
                    <h6 className={classes.description}>CRN</h6>
                  </CardBody>
                </Card>
              </ScrollAnimation>
            </GridItem>
          </GridContainer>
        </div>
      </div>

      <div
        className={`${classes.projects} ${classes.projects4}`}
        id="statistics"
      >
        <div className={classes.container}>
          <GridContainer>
            <GridItem
              md={12}
              className={`${classes.mlAuto} ${classes.mrAuto} ${classes.textCenter}`}
            >
              <h2 className={classes.title}>Statistics</h2>
              <h4 className={classes.description}>
                Fleet optimization includes consolidating redundant devices,
                reducing paper consumption by usage of the duplex printing
                enabling a decrease in the carbon footprint of the organization.
              </h4>
              <div className={classes.sectionSpace} />
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={5} md={5} className={classes.mlAuto}>
              <ScrollAnimation animateIn="fadeInUp">
                <InfoArea
                  className={classes.info4}
                  title="500 No. of clients"
                  description="lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum."
                  icon={Group}
                  iconColor="rose"
                />
              </ScrollAnimation>
              <ScrollAnimation animateIn="fadeInUp">
                <InfoArea
                  className={classes.info4}
                  title="25000 Machine Installed"
                  description="lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum"
                  icon={Code}
                  iconColor="success"
                />
              </ScrollAnimation>
              <ScrollAnimation animateIn="fadeInUp">
                <InfoArea
                  className={classes.info4}
                  title="99% Rentention Rate"
                  description="lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum"
                  icon={Timeline}
                  iconColor="info"
                />
              </ScrollAnimation>
            </GridItem>
            <GridItem xs={12} sm={5} md={5} className={classes.mrAuto}>
              <ScrollAnimation animateIn="fadeInRight">
                <Card
                  background
                  className={classes.card4}
                  style={{
                    backgroundImage: `url(https://miro.medium.com/max/720/0*UjBJ_iTNESi6Zevk.jpg)`,
                  }}
                >
                  <CardBody background className={classes.cardBody4}>
                    <Badge color="rose">lorem ipsumlorem ipsumlorem</Badge>
                    <a href="#stats" onClick={(e) => e.preventDefault}>
                      <h3 className={classes.cardTitle}>Another One</h3>
                      <p className={classes.cardDescription}>
                        lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem
                        ipsumlorem ipsum
                      </p>
                    </a>
                  </CardBody>
                </Card>
              </ScrollAnimation>
            </GridItem>
          </GridContainer>
        </div>
      </div>
      {/* Project 4 END */}
    </div>
  );
}

export default withStyles(projectsStyle)(SectionProjects);

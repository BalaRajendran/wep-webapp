import React from "react";
// nodejs library that concatenates classes
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
// core components
import Header from "./../../../components/Header/Header.jsx";
import Button from "./../../../components/CustomButtons/Button.jsx";

import headersStyle from "./../../../assets/jss/material-kit-pro-react/views/sectionsSections/headersStyle.jsx";

function SectionHeaders({ ...props }) {
  const { classes } = props;
  return (
    <Header
      absolute
      brand="/logo512.png"
      color="transparent"
      links={
        <div className={classes.collapse}>
          <List className={classes.list + " " + classes.mlAuto}>
            <ListItem className={classes.listItem}>
              <Button
                href="#pablo"
                className={classes.navLink}
                onClick={(e) => e.preventDefault()}
                color="transparent"
              >
                <i className="Blink">
                  <FiberManualRecordIcon style={{ color: "green" }} />
                </i>{" "}
                Home
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="#pablo"
                className={classes.navLink}
                onClick={(e) => e.preventDefault()}
                color="transparent"
              >
                About us
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="#pablo"
                className={classes.navLink}
                onClick={(e) => e.preventDefault()}
                color="transparent"
              >
                Print Solutions
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="#pablo"
                className={classes.navLink}
                onClick={(e) => e.preventDefault()}
                color="transparent"
              >
                Case Studies
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                href="#pablo"
                className={classes.navLink}
                onClick={(e) => e.preventDefault()}
                color="transparent"
              >
                Contact Us
              </Button>
            </ListItem>
          </List>
          <List className={classes.list + " " + classes.mlAuto}>
            <ListItem className={classes.listItem}>
              <Button
                color="transparent"
                href="https://www.facebook.com/wepmpsexpert/"
                target="_blank"
                className={`${classes.navLink} ${classes.navLinkJustIcon}`}
              >
                <i className={"fab fa-facebook"} />
              </Button>
            </ListItem>
            <ListItem className={classes.listItem}>
              <Button
                color="transparent"
                href="https://www.linkedin.com/company/wep-solutions-india-limited/"
                target="_blank"
                className={`${classes.navLink} ${classes.navLinkJustIcon}`}
              >
                <i className={"fab fa-linkedin"} />
              </Button>
            </ListItem>
          </List>
        </div>
      }
    />
  );
}

export default withStyles(headersStyle)(SectionHeaders);

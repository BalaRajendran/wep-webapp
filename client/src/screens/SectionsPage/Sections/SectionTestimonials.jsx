import React from "react";
// react component for creating beautiful carousel
import Carousel from "react-slick";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import Star from "@material-ui/icons/Star";
// core components
import GridContainer from "./../../../components/Grid/GridContainer.jsx";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import Card from "./../../../components/Card/Card.jsx";
import CardBody from "./../../../components/Card/CardBody.jsx";
// import CardAvatar from "./../../../components/Card/CardAvatar.jsx";
// import Muted from "./../../../components/Typography/Muted.jsx";
import Warning from "./../../../components/Typography/Warning.jsx";

import testimonialsStyle from "./../../../assets/jss/material-kit-pro-react/views/sectionsSections/testimonialsStyle.jsx";

// import kendall from "./../../../assets/img/faces/kendall.jpg";
// import christian from "./../../../assets/img/faces/christian.jpg";

function SectionTestimonials({ ...props }) {
  const { classes, ...rest } = props;
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
  };
  return (
    <div className="cd-section" {...rest} id="testimonals">
      <div
        className={`${classes.testimonials} ${classes.sectionDark} ${classes.testimonial2}`}
      >
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <Carousel {...settings}>
                <div>
                  <Card testimonial plain className={classes.card2}>
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <i
                        className="fas fa-quote-left"
                        style={{ fontSize: "40px" }}
                      ></i>
                    </a>
                    <CardBody plain>
                      <h5 className={classes.cardDescription}>
                        Being a company of more than 2, 00,000 employees, we
                        were looking for a robust technology to support ongoing
                        growth and help employees to be focussed on other
                        critical activities. We approached WeP to check what
                        they had in store. We were impressed with their service.
                        They helped us with timely services and billing. Also,
                        reduced our management costs and controlling costs and
                        improving efficiency"
                      </h5>
                      <h4 className={classes.cardTitle}>
                        Leading FMCG Company
                      </h4>
                    </CardBody>
                    <div>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                    </div>
                  </Card>
                </div>
                <div>
                  <Card testimonial plain className={classes.card2}>
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <i
                        className="fas fa-quote-left"
                        style={{ fontSize: "40px" }}
                      ></i>
                    </a>
                    <CardBody plain>
                      <h5 className={classes.cardDescription}>
                        We compared our general printing costs (including toner
                        cartridges) with WeP’s MPS package and the results were
                        impressing. We were also attracted to the dedication of
                        the team and service level they offer"
                      </h5>
                      <h4 className={classes.cardTitle}>
                        Leading Airline Company
                      </h4>
                    </CardBody>
                    <div>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                    </div>
                  </Card>
                </div>
                <div>
                  <Card testimonial plain className={classes.card2}>
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <i
                        className="fas fa-quote-left"
                        style={{ fontSize: "40px" }}
                      ></i>
                    </a>
                    <CardBody plain>
                      <h5 className={classes.cardDescription}>
                        We live in a digital world today. But, as an insurance
                        company, we still rely on paperwork to some extent. And
                        it is crucial for insurance companies to manage their
                        printing environment and automate paper intensive
                        workflows. Managing their fleet holistically will help
                        in better results and accelerate business. WeP did the
                        same for us and we are satisfied.”
                      </h5>
                      <h4 className={classes.cardTitle}>
                        Leading Insurance Company
                      </h4>
                    </CardBody>
                    <div>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                      <Warning>
                        <Star className={classes.starIcons} />
                      </Warning>
                    </div>
                  </Card>
                </div>
              </Carousel>
            </GridItem>
          </GridContainer>
        </div>
      </div>
    </div>
  );
}

export default withStyles(testimonialsStyle)(SectionTestimonials);

import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import AssessmentIcon from "@material-ui/icons/Assessment";
// import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import WbIncandescentIcon from "@material-ui/icons/WbIncandescent";
import InfoIcon from "@material-ui/icons/Info";
import ScrollAnimation from "react-animate-on-scroll";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import CompareArrowsIcon from "@material-ui/icons/CompareArrows";
// import PrintIcon from '@material-ui/icons/Policy';
import PrintIcon from "@material-ui/icons/Print";
// import EcoIcon from '@material-ui/icons/Eco';
import PaymentIcon from "@material-ui/icons/Payment";
import SecurityIcon from "@material-ui/icons/Security";
import AutorenewIcon from "@material-ui/icons/Autorenew";
import FlagIcon from "@material-ui/icons/Flag";
import LaunchIcon from "@material-ui/icons/Launch";
import RemoveFromQueueIcon from "@material-ui/icons/RemoveFromQueue";
// import AccountTreeIcon from '@material-ui/icons/AccountTree';
import AccountTreeIcon from "@material-ui/icons/SettingsInputAntenna";
import TimelineIcon from "@material-ui/icons/Timeline";
// import EmojiNatureIcon from '@material-ui/icons/EmojiNature';
import EmojiNatureIcon from "@material-ui/icons/NaturePeople";
// core components
import GridContainer from "./../../../components/Grid/GridContainer.jsx";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import InfoArea from "./../../../components/InfoArea/InfoArea.jsx";

import featuresStyle from "./../../../assets/jss/material-kit-pro-react/views/sectionsSections/featuresStyle.jsx";

// import iphone2 from "./../../../assets/img/sections/iphone2.png";
import iphone2 from "./../../../assets/img/how_works.png";

function SectionFeatures({ ...props }) {
  let grid1 = [];
  let grid2 = [];
  [
    {
      icon: AssessmentIcon,
      title: "Need Assessment & Due Diligence",
      description:
        "Determine optimum future state through insights from fact-based analysis of current state",
      iconColor: "primary",
    },
    {
      icon: InfoIcon,
      title: "Plan & Propose",
      description:
        "Make a viable long term plan. Set expectations and show the way.",
      iconColor: "info",
    },
    {
      icon: VerifiedUser,
      title: "Deploy & Transition",
      description:
        "Implement solutions seamlessly with minimal impact and maximum organizational benefit.",
      iconColor: "success",
    },
    {
      icon: WbIncandescentIcon,
      title: "Manage & Innovate",
      description:
        "Increase efficiency and improve workflow to keep your business working—today and tomorrow.",
      iconColor: "warning",
    },
  ].map((value, i) => {
    if (i < 2)
      return grid1.push(
        <ScrollAnimation key={i} animateIn="fadeInUp">
          <InfoArea
            icon={value.icon}
            title={value.title}
            description={value.description}
            iconColor={value.iconColor}
          />
        </ScrollAnimation>
      );
    else
      return grid2.push(
        <ScrollAnimation key={i} animateIn="fadeInUp">
          <InfoArea
            icon={value.icon}
            title={value.title}
            description={value.description}
            iconColor={value.iconColor}
          />
        </ScrollAnimation>
      );
  });

  let benifits = [];
  [
    {
      icon: CompareArrowsIcon,
      title: "Direct & Indirect Cost Reduction",
      iconColor: "danger",
    },
    {
      icon: PrintIcon,
      title: "Define & Implement print policy",
      iconColor: "info",
    },
    {
      icon: EmojiNatureIcon,
      title: "Reduce Environmental Impact",
      iconColor: "success",
    },
    {
      icon: PaymentIcon,
      title: "Pay for what you Print",
      iconColor: "gray",
    },
    {
      icon: SecurityIcon,
      title: "Enhance Print Security",
      iconColor: "success",
    },
    {
      icon: RemoveFromQueueIcon,
      title: "Eliminate multiple channel contracts",
      iconColor: "primary",
    },
    {
      icon: AutorenewIcon,
      title: "Helps to print & Eliminate wastage",
      iconColor: "success",
    },
    {
      icon: FlagIcon,
      title: "Customised solution to meet PAN India presence",
      iconColor: "warning",
    },
    {
      icon: LaunchIcon,
      title: "Single window solution",
      iconColor: "rose",
    },
    {
      icon: AccountTreeIcon,
      title: "Proactive management of print Infrastructure",
      iconColor: "info",
    },
    {
      icon: TimelineIcon,
      title: "Enhance user experience & Increase Printer availability",
      iconColor: "success",
    },
    {
      icon: WbIncandescentIcon,
      title: "Manage & Innovate",
      iconColor: "warning",
    },
  ].map((value, i) => {
    return benifits.push(
      <GridItem key={i} xs={12} sm={4} md={2}>
        <ScrollAnimation animateIn="flipInX">
          <InfoArea
            vertical
            icon={value.icon}
            description={value.title}
            iconColor={value.iconColor}
            styles={{fontWeight: 600}}
          />
        </ScrollAnimation>
      </GridItem>
    );
  });
  const { classes, ...rest } = props;
  return (
    <div className="cd-section" {...rest}>
      <div className={classes.container} id="howitworks">
        {/* Feature 1 START */}
        <div
          className={classes.features4}
          style={{ paddingTop: 0, paddingBottom: 0 }}
        >
          <GridContainer>
            <GridItem
              md={12}
              className={`${classes.mlAuto} ${classes.mrAuto} ${classes.textCenter}`}
            >
              <h2 className={classes.title}>How it works</h2>
              <h4 className={classes.description}>
                <ScrollAnimation animateIn="pulse">
                  WeP Solutions not only provides MPS by deploying various types
                  of imaging devices, but also provides MPS on the existing
                  devices of the clients. A thorough assessment of the existing
                  print infrastructure is done prior to deployment. All the
                  solutions are highly customized based on the customer's need
                  and the requirement to help reduce costs and have a greater
                  ROI.
                </ScrollAnimation>
              </h4>
            </GridItem>
          </GridContainer>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12} lg={3} className={classes.mlAuto}>
              {grid1}
            </GridItem>
            <GridItem xs={12} sm={12} md={12} lg={4}>
              <ScrollAnimation animateIn="fadeIn">
                <div className={classes.phoneContainer}>
                  <img
                    src={iphone2}
                    alt="..."
                    style={{
                      boxShadow:
                        "0 5px 15px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
                      height: "410px",
                    }}
                  />
                </div>
              </ScrollAnimation>
            </GridItem>
            <GridItem xs={12} sm={12} md={12} lg={3} className={classes.mrAuto}>
              {grid2}
            </GridItem>
          </GridContainer>
        </div>
        {/* Feature 1 END */}
      </div>
      <div className={classes.container} id="mpsbenefits">
        <div
          className={classes.features1}
          style={{ paddingTop: "95px", paddingBottom: 0 }}
        >
          <GridContainer>
            <GridItem md={10} className={`${classes.mlAuto} ${classes.mrAuto}`}>
              <h2 className={classes.title}>MPS Benefits</h2>
              <h4 className={classes.description}>
                With the supply of eco-friendly printers, monitored and
                controlled printing environment, and processing of e-waste
                ensuring used printers, consumables, spares do not go to the
                landfill if partnered with a professional e-waste management
                company. MPS helps organizations save costs, reduce wastage and
                manpower and increase productivity and ROI.
              </h4>
            </GridItem>
          </GridContainer>
        </div>
        <div
          className={classes.features1}
          style={{ paddingTop: 0, paddingBottom: 0 }}
        >
          <GridContainer>{benifits}</GridContainer>
        </div>
      </div>
    </div>
  );
}

export default withStyles(featuresStyle)(SectionFeatures);

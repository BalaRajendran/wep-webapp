import React, { Component } from "react";
import Footer from "../../../components/Footer/Footer.jsx";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Favorite from "@material-ui/icons/Favorite";
import withStyles from "@material-ui/core/styles/withStyles";
import SectionFooterStyles from "./../../../assets/jss/material-kit-pro-react/views/SectionFooterStyle.jsx";
import GridContainer from "./../../../components/Grid/GridContainer.jsx";
import GridItem from "./../../../components/Grid/GridItem.jsx";
import Card from "./../../../components/Card/Card.jsx";
var array = [
  "Anti fraud policy",
  "carees",
  "contact us",
  "corporate health claim",
  "disclaimer",
  "dnda registration",
  "downloads",
  "employee loogin",
  "faq",
  "free look period",
  "GI council",
  "insurance information buerou",
  "glossary",
  "rievence",
  "health insurance for corporate",
  "health zone",
  "home",
  "irdai",
  "irdai consuumer education website",
  "irdai regulations appoinnment",
  "lab login",
  "link adharr/pin",
  "potability",
  "privacy",
  "public disclouser",
  "service parameter",
  "sitemap",
  "tax benifits"
];
class SectionFooter extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Card style={{ width: "750px" }}>
          <GridContainer lg={12}>
            <GridItem lg={2}>
              {array.slice(0, 7).map(props => {
                return (
                  <div>
                    <p style={{ fontSize: "12px" }}>{props}</p>
                    <p></p>
                  </div>
                );
              })}
            </GridItem>
            <GridItem lg={2}>
              {array.slice(7, 14).map(props => {
                return (
                  <div>
                    <p style={{ fontSize: "12px" }}>{props}</p>
                    <p></p>
                  </div>
                );
              })}
            </GridItem>
            <GridItem lg={2}>
              {array.slice(0, 7).map(props => {
                return (
                  <div>
                    <p style={{ fontSize: "12px" }}>{props}</p>
                    <p></p>
                  </div>
                );
              })}
            </GridItem>
            <GridItem lg={2}>
              {array.slice(0, 7).map(props => {
                return (
                  <div>
                    <p style={{ fontSize: "12px" }}>{props}</p>
                    <p></p>
                  </div>
                );
              })}
            </GridItem>
            <GridItem lg={2}>
              {array.slice(0, 7).map(props => {
                return (
                  <div>
                    <p style={{ fontSize: "12px" }}>{props}</p>
                    <p></p>
                  </div>
                );
              })}
            </GridItem>
            <GridItem lg={2}>
              {array.slice(0, 7).map(props => {
                return (
                  <div>
                    <p style={{ fontSize: "12px" }}>{props}</p>
                    <p></p>
                  </div>
                );
              })}
            </GridItem>
          </GridContainer>
        </Card>
      </div>
    );
  }
}

export default withStyles(SectionFooterStyles)(SectionFooter);
